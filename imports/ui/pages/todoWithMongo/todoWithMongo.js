import './todoWithMongo.html'
import {Random} from 'meteor/random'

const Todos = new Mongo.Collection(null)

Template.todoWithMongo.onCreated(function (){
    Todos.remove({})
    // Todos.insert({_id: Random.id(),text:'test',done:false})
})

Template.todoWithMongo.helpers({
    todos(){
        return Todos.find()
    }
})

Template.todoWithMongo.events({
    'submit #addTodo'(e,temp){
        e.preventDefault();
        Todos.insert({_id: Random.id(),text: e.target.text.value,done:false})
    },
    'click .removeTodo'(e,temp){
        Todos.remove({_id: this._id})
    }
})
