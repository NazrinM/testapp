import './main.html'
import {Random} from 'meteor/random'
Template.home.onCreated(function (){
    this.todos=new ReactiveVar([{_id:Random.id(),text:'text',done: false}])
})

Template.home.helpers({
    todos(){
        return Template.instance().todos.get()
    }
})

Template.home.events({
    'submit #addTodo'(e,temp){
        e.preventDefault();
        let target=e.target;
        let text=target.text.value;
        let todos=[...temp.todos.get()];
        todos.push({_id: Random.id(),text,done:false})
        temp.todos.set(todos)
    },
    'click .removeTodo'(e,temp){
        let todos=[...temp.todos.get()], todoId=this._id;
        let index=todos.findIndex((el)=>(el._id===todoId));
        todos.splice(index,1)
        temp.todos.set(todos)
    }
})
