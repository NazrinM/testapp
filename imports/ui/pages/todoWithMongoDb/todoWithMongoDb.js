import './todoWithMongoDb.html'
import {Todos} from "../../../api/todos/collections";

Template.todoWithMongoDb.onCreated(function (){
    this.autorun(()=>{
        this.subscribe('allTodos',false)
    })
})

Template.todoWithMongoDb.helpers({
    todos(){return Todos.find()}
})

Template.todoWithMongoDb.events({
    'submit #addTodo'(e,temp){
        e.preventDefault();
        Meteor.call('addTodo',e.target.text.value,(err,res)=>{
            if(res) alert(res)
        })
    }
})


