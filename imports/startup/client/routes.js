/** needed files **/
import '../../ui/layouts/layout'
import '../../ui/pages/main'
import '../../ui/pages/todoWithMongo/todoWithMongo'
import '../../ui/pages/todoWithMongoDb/todoWithMongoDb'

import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

FlowRouter.route('/', {
    name: 'index',
    action() {
        BlazeLayout.render('layout', { content: "home" });
    }
});

FlowRouter.route('/todoWithMongo', {
    name: 'todoWithMongo',
    action() {
        BlazeLayout.render('layout', { content: "todoWithMongo" });
    }
});

FlowRouter.route('/todoWithMongoDb', {
    name: 'todoWithMongoDb',
    action() {
        BlazeLayout.render('layout', { content: "todoWithMongoDb" });
    }
});
