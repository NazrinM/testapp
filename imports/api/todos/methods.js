import {Todos} from "./collections";

Meteor.methods({
    addTodo(text){
        return Todos.insert({text,done: false})
    },
    removeTodo(_id){
        return Todos.remove({_id})
    }
})
